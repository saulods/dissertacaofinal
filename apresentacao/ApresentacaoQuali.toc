\beamer@endinputifotherversion {3.36pt}
\select@language {brazil}
\beamer@sectionintoc {1}{sumario}{2}{0}{1}
\beamer@sectionintoc {2}{Introdu\IeC {\c c}\IeC {\~a}o}{3}{0}{2}
\beamer@subsectionintoc {2}{1}{Contextualiza\IeC {\c c}\IeC {\~a}o do tema}{3}{0}{2}
\beamer@sectionintoc {3}{Problema de pesquisa}{5}{0}{3}
\beamer@subsectionintoc {3}{1}{Problema problema}{5}{0}{3}
\beamer@subsectionintoc {3}{2}{hip\IeC {\'o}teses de resolu\IeC {\c c}\IeC {\~a}o do problema}{9}{0}{3}
\beamer@subsectionintoc {3}{3}{Objetivos geral e espec\IeC {\'\i }ficos}{10}{0}{3}
\beamer@sectionintoc {4}{Conceitos}{12}{0}{4}
\beamer@subsubsectionintoc {4}{0}{1}{Web-Crawlers}{13}{0}{4}
\beamer@subsectionintoc {4}{1}{Ferramenta de extra\IeC {\c c}\IeC {\~o}es de dados na plataforma Lattes}{18}{0}{4}
\beamer@subsectionintoc {4}{2}{Principais trabalhos}{19}{0}{4}
\beamer@sectionintoc {5}{ Agrupamento Hier\IeC {\'a}rquico}{20}{0}{5}
\beamer@subsectionintoc {5}{1}{Agrupamento Hier\IeC {\'a}rquico de curr\IeC {\'\i }culos Lattes}{22}{0}{5}
\beamer@subsectionintoc {5}{2}{aplica\IeC {\c c}\IeC {\~o}es}{31}{0}{5}
\beamer@sectionintoc {6}{Planos para defesa}{35}{0}{6}
\beamer@sectionintoc {7}{Planos para defesa}{36}{0}{7}
