\select@language {brazil}
\contentsline {chapter}{Lista de Figuras}{7}{chapter*.2}
\contentsline {chapter}{Lista de Tabelas}{8}{chapter*.3}
\contentsline {chapter}{Lista de S\IeC {\'\i }mbolos}{9}{chapter*.4}
\contentsline {chapter}{\numberline {1}Aplica\IeC {\c c}\IeC {\~o}es e resultados}{11}{chapter.1}
\contentsline {section}{\numberline {1.1}Hierarquia de curr\IeC {\'\i }culos}{11}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Parametriza\IeC {\c c}\IeC {\~a}o da hierarquia}{12}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Gera\IeC {\c c}\IeC {\~a}o de Scripts}{14}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Gera\IeC {\c c}\IeC {\~a}o de relat\IeC {\'o}rios}{16}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Aplica\IeC {\c c}\IeC {\~a}o para cadastro da plataforma Sucupira}{16}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Adapta\IeC {\c c}\IeC {\~a}o na parametriza\IeC {\c c}\IeC {\~a}o da hierarquia para o ScriptSucupira}{17}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Resultados utilizando aplica\IeC {\c c}\IeC {\~a}o ScriptSucupira}{21}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Aplica\IeC {\c c}\IeC {\~a}o para cadastro da plataforma e-Mec}{21}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Resultados utilizando as ferramentas ScriptSucupira e ScriptEMec}{23}{subsection.1.3.1}
\contentsline {section}{\numberline {1.4}ScriptComp}{23}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Resultados utilizando a ferramenta ScriptComp}{28}{subsection.1.4.1}
\contentsline {chapter}{Refer\^encias Bibliogr\'aficas}{33}{chapter*.20}
\contentsfinish 
