\select@language {brazil}
\contentsline {chapter}{Lista de Figuras}{8}{chapter*.2}
\contentsline {chapter}{Lista de Tabelas}{10}{chapter*.3}
\contentsline {chapter}{Lista de S\IeC {\'\i }mbolos}{11}{chapter*.4}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{13}{chapter.1}
\contentsline {section}{\numberline {1.1}Contextualiza\IeC {\c c}\IeC {\~a}o do tema}{13}{section.1.1}
\contentsline {section}{\numberline {1.2}Problema de pesquisa}{14}{section.1.2}
\contentsline {section}{\numberline {1.3}Objetivos}{15}{section.1.3}
\contentsline {section}{\numberline {1.4}Estrutura do trabalho}{16}{section.1.4}
\contentsline {chapter}{\numberline {2}Web-Crawlers para extra\IeC {\c c}\IeC {\~a}o de dados na plataforma Lattes}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Web-Crawlers}{17}{section.2.1}
\contentsline {section}{\numberline {2.2}Ferramentas para extra\IeC {\c c}\IeC {\~o}es de dados na plataforma Lattes e Web-Crawlers}{18}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}OntoLattes}{18}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}GeraLattes}{19}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}SemanticLattes}{19}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}LattesMiner}{20}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Lattes Extrator}{21}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}ScriptLattes}{21}{subsection.2.2.6}
\contentsline {section}{\numberline {2.3}Utilizando ScriptLattes para a extra\IeC {\c c}\IeC {\~o}es de curr\IeC {\'\i }culos e minera\IeC {\c c}\IeC {\~o}es de informa\IeC {\c c}\IeC {\~o}es}{25}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Trabalhos que utilizam o ScriptLattes}{26}{subsection.2.3.1}
\contentsline {chapter}{\numberline {3}Agrupamentos hier\IeC {\'a}rquicos a partir de relat\IeC {\'o}rios gerados pelo ScriptLattes}{28}{chapter.3}
\contentsline {section}{\numberline {3.1}Agrupamento Hier\IeC {\'a}rquico}{28}{section.3.1}
\contentsline {section}{\numberline {3.2}Agrupamento Hier\IeC {\'a}rquico de curr\IeC {\'\i }culos Lattes}{29}{section.3.2}
\contentsline {chapter}{\numberline {4}Aplica\IeC {\c c}\IeC {\~o}es e resultados}{33}{chapter.4}
\contentsline {section}{\numberline {4.1}ScriptSucupira}{33}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Resultados utilizando o ScriptSucupira}{37}{subsection.4.1.1}
\contentsline {section}{\numberline {4.2}ScriptEMec}{43}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Resultados utilizando a ferramenta ScriptEMec}{46}{subsection.4.2.1}
\contentsline {section}{\numberline {4.3}ScriptComp}{51}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Resultados utilizando a ferramenta ScriptComp}{56}{subsection.4.3.1}
\contentsline {chapter}{\numberline {5}Conclus\IeC {\~a}o}{61}{chapter.5}
\contentsline {section}{\numberline {5.1}Conclus\IeC {\~a}o}{61}{section.5.1}
\contentsline {section}{\numberline {5.2}Trabalhos Futuros}{61}{section.5.2}
\contentsline {chapter}{Refer\^encias Bibliogr\'aficas}{67}{chapter*.53}
\contentsfinish 
