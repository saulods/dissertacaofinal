\select@language {brazil}
\contentsline {section}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{9}{section.1}
\contentsline {subsection}{\numberline {1.1}Contextualiza\IeC {\c c}\IeC {\~a}o do tema}{9}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Problema de pesquisa}{11}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Objetivos}{12}{subsection.1.3}
\contentsline {subsubsection}{\numberline {1.3.1}Objetivo geral}{12}{subsubsection.1.3.1}
\contentsline {subsubsection}{\numberline {1.3.2}Objetivos espec\IeC {\'\i }ficos}{12}{subsubsection.1.3.2}
\contentsline {subsection}{\numberline {1.4}Estrutura do trabalho}{13}{subsection.1.4}
\contentsline {section}{\numberline {2}Web-Crawlers para extra\IeC {\c c}\IeC {\~a}o de dados na plataforma Lattes}{14}{section.2}
\contentsline {subsection}{\numberline {2.1}Web-Crawlers}{14}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Ferramentas de extra\IeC {\c c}\IeC {\~o}es da Plataforma Lattes e Web-Crawlers}{15}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}OntoLattes}{15}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}GeraLattes}{16}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}SemanticLattes}{16}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}ScriptLattes}{17}{subsubsection.2.2.4}
\contentsline {subsection}{\numberline {2.3}Utilizando ScriptLattes para extra\IeC {\c c}\IeC {\~a}o dos curr\IeC {\'\i }culos e minera\IeC {\c c}\IeC {\~a}o das informa\IeC {\c c}\IeC {\~o}es da Plataforma Lattes}{21}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Trabalhos que utilizam o ScriptLattes}{22}{subsubsection.2.3.1}
\contentsline {section}{\numberline {3}Agrupamentos hier\IeC {\'a}rquicos a partir de relat\IeC {\'o}rios gerados pelo ScriptLattes}{25}{section.3}
\contentsline {subsubsection}{\numberline {3.0.1}Agrupamento Hier\IeC {\'a}rquico}{25}{subsubsection.3.0.1}
\contentsline {subsubsection}{\numberline {3.0.2}Agrupamento Hier\IeC {\'a}rquico de curr\IeC {\'\i }culos Lattes}{26}{subsubsection.3.0.2}
\contentsline {section}{\numberline {4}Aplica\IeC {\c c}\IeC {\~o}es}{30}{section.4}
\contentsline {subsection}{\numberline {4.1}ScriptSucupira}{30}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}ScriptEmec}{31}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Classifica\IeC {\c c}\IeC {\~a}o do conceito CAPES}{31}{subsection.4.3}
\contentsline {section}{\numberline {5}Conclus\IeC {\~a}o}{33}{section.5}
\contentsline {subsection}{\numberline {5.1}Resultados}{33}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Trabalhos relacionados}{33}{subsection.5.2}
\contentsline {section}{REFER\IeC {\^E}NCIAS BIBLIOGR\IeC {\'A}FICAS}{34}{subsection.5.2}
