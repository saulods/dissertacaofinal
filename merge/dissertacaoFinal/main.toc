\select@language {brazil}
\contentsline {chapter}{Lista de Figuras}{8}{chapter*.2}
\contentsline {chapter}{Lista de Tabelas}{9}{chapter*.3}
\contentsline {chapter}{Lista de S\IeC {\'\i }mbolos}{10}{chapter*.4}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{12}{chapter.1}
\contentsline {section}{\numberline {1.1}Contextualiza\IeC {\c c}\IeC {\~a}o do tema}{12}{section.1.1}
\contentsline {section}{\numberline {1.2}Problema de pesquisa}{13}{section.1.2}
\contentsline {section}{\numberline {1.3}Objetivos}{14}{section.1.3}
\contentsline {section}{\numberline {1.4}Estrutura do trabalho}{15}{section.1.4}
\contentsline {chapter}{\numberline {2}Web-Crawlers para extra\IeC {\c c}\IeC {\~a}o de dados na plataforma Lattes}{16}{chapter.2}
\contentsline {section}{\numberline {2.1}Web-Crawlers}{16}{section.2.1}
\contentsline {section}{\numberline {2.2}Ferramentas para extra\IeC {\c c}\IeC {\~o}es de dados na plataforma Lattes e Web-Crawlers}{17}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}OntoLattes}{17}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}GeraLattes}{18}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}SemanticLattes}{18}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}LattesMiner}{19}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Lattes Extrator}{20}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}ScriptLattes}{20}{subsection.2.2.6}
\contentsline {section}{\numberline {2.3}Utilizando ScriptLattes para a extra\IeC {\c c}\IeC {\~o}es de curr\IeC {\'\i }culos e minera\IeC {\c c}\IeC {\~o}es de informa\IeC {\c c}\IeC {\~o}es}{24}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Trabalhos que utilizam o ScriptLattes}{25}{subsection.2.3.1}
\contentsline {chapter}{\numberline {3}Agrupamentos hier\IeC {\'a}rquicos a partir de relat\IeC {\'o}rios gerados pelo ScriptLattes}{27}{chapter.3}
\contentsline {section}{\numberline {3.1}Agrupamento de Curr\IeC {\'\i }culo Lattes}{27}{section.3.1}
\contentsline {section}{\numberline {3.2}Hierarquia de Curr\IeC {\'\i }culos}{27}{section.3.2}
\contentsline {section}{\numberline {3.3}Agrupamento Hier\IeC {\'a}rquico}{28}{section.3.3}
\contentsline {chapter}{\numberline {4}Aplica\IeC {\c c}\IeC {\~o}es e resultados}{33}{chapter.4}
\contentsline {section}{\numberline {4.1}Hierarquia de curr\IeC {\'\i }culos}{33}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Parametriza\IeC {\c c}\IeC {\~a}o da hierarquia}{34}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Gera\IeC {\c c}\IeC {\~a}o de Scripts}{36}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Gera\IeC {\c c}\IeC {\~a}o de relat\IeC {\'o}rios}{38}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Aplica\IeC {\c c}\IeC {\~a}o para cadastro da plataforma Sucupira}{38}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Adapta\IeC {\c c}\IeC {\~a}o na parametriza\IeC {\c c}\IeC {\~a}o da hierarquia para o ScriptSucupira}{39}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Resultados utilizando aplica\IeC {\c c}\IeC {\~a}o ScriptSucupira}{43}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Aplica\IeC {\c c}\IeC {\~a}o para cadastro da plataforma e-Mec}{43}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Resultados utilizando as ferramentas ScriptSucupira e ScriptEMec}{45}{subsection.4.3.1}
\contentsline {section}{\numberline {4.4}ScriptComp}{45}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Resultados utilizando a ferramenta ScriptComp}{50}{subsection.4.4.1}
\contentsline {chapter}{\numberline {5}Conclus\IeC {\~a}o}{55}{chapter.5}
\contentsline {section}{\numberline {5.1}Conclus\IeC {\~a}o}{55}{section.5.1}
\contentsline {section}{\numberline {5.2}Trabalhos Futuros}{55}{section.5.2}
\contentsline {chapter}{Refer\^encias Bibliogr\'aficas}{57}{chapter*.30}
\contentsfinish 
