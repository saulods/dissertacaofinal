\select@language {brazil}
\contentsline {section}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{6}{section.1}
\contentsline {subsection}{\numberline {1.1}Contextualiza\IeC {\c c}\IeC {\~a}o do tema}{6}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Identifica\IeC {\c c}\IeC {\~a}o de lacunas ou quest\IeC {\~o}es de pesquisa }{7}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Delimita\IeC {\c c}\IeC {\~a}o do tema }{8}{subsection.1.3}
\contentsline {section}{\numberline {2}Problema de pesquisa}{10}{section.2}
\contentsline {subsection}{\numberline {2.1}Situa\IeC {\c c}\IeC {\~a}o problema (conceitos centrais estudados e o problema de pesquisa)}{10}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Proposi\IeC {\c c}\IeC {\~o}es / hip\IeC {\'o}teses de resolu\IeC {\c c}\IeC {\~a}o do problema}{10}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Objetivos geral e espec\IeC {\'\i }ficos}{11}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Objetivo geral}{11}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Objetivos espec\IeC {\'\i }ficos}{11}{subsubsection.2.3.2}
\contentsline {subsection}{\numberline {2.4}Justificativa da pesquisa (relev\IeC {\^a}ncia, import\IeC {\^a}ncia e atualidade)}{11}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Modelo te\IeC {\'o}rico (constructo te\IeC {\'o}rico previsto)}{12}{subsection.2.5}
\contentsline {subsubsection}{\numberline {2.5.1}Agrupamento Hier\IeC {\'a}rquico dos dados}{12}{subsubsection.2.5.1}
\contentsline {subsubsection}{\numberline {2.5.2}Compara\IeC {\c c}\IeC {\~a}o e Predi\IeC {\c c}\IeC {\~a}o}{16}{subsubsection.2.5.2}
\contentsline {subsection}{\numberline {2.6}Delimita\IeC {\c c}\IeC {\~a}o da pesquisa}{16}{subsection.2.6}
\contentsline {section}{\numberline {3}Plataforma te\IeC {\'o}rica prevista}{17}{section.3}
\contentsline {subsection}{\numberline {3.1}Principais t\IeC {\'o}picos a serem desenvolvidos}{17}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Principais autores (seminais, estado da arte e autor-base de cada t\IeC {\'o}pico)}{17}{subsection.3.2}
\contentsline {section}{\numberline {4}M\IeC {\'e}todos e instrumentos previstos}{20}{section.4}
\contentsline {subsection}{\numberline {4.1}Escolha e justificativa da tipologia da pesquisa (natureza, abordagem , objetivo e m\IeC {\'e}todo de pesquisa)}{20}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Universo, amostragem, amostra}{20}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}T\IeC {\'e}cnicas de coleta e tratamento de dados previstas}{20}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Limita\IeC {\c c}\IeC {\~o}es da pesquisa}{21}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Modelo te\IeC {\'o}rico-emp\IeC {\'\i }rico previsto (constructo te\IeC {\'o}rico-aplicado previsto)}{21}{subsection.4.5}
\contentsline {section}{\numberline {5}Apresenta\IeC {\c c}\IeC {\~a}o, an\IeC {\'a}lise e discuss\IeC {\~a}o dos resultados}{25}{section.5}
\contentsline {subsection}{\numberline {5.1}Resultados preliminares}{25}{subsection.5.1}
\contentsline {section}{\numberline {6}CRONOGRAMA}{26}{section.6}
\contentsline {section}{REFER\IeC {\^E}NCIAS BIBLIOGR\IeC {\'A}FICAS}{28}{table.2}
