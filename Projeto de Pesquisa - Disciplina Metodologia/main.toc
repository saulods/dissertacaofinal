\select@language {brazil}
\contentsline {section}{\numberline {1}INTRODU\IeC {\c C}\IeC {\~A}O}{5}{section.1}
\contentsline {subsection}{\numberline {1.1}Plataforma Lattes}{5}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Extra\IeC {\c c}\IeC {\~a}o de informa\IeC {\c c}\IeC {\~o}es utilizando a ferramenta ScriptLattes}{6}{subsection.1.2}
\contentsline {section}{\numberline {2}FORMULA\IeC {\c C}\IeC {\~A}O DO PROBLEMA}{9}{section.2}
\contentsline {subsection}{\numberline {2.1}Formula\IeC {\c c}\IeC {\~a}o do Problema}{9}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Objetivos}{10}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Delimita\IeC {\c c}\IeC {\~a}o do estudo}{11}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Justificativas e contribui\IeC {\c c}\IeC {\~o}es da pesquisa}{11}{subsection.2.4}
\contentsline {section}{\numberline {3}METODOLOGIA E PROCEDIMENTOS DE PESQUISA}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Escolha do m\IeC {\'e}todo a ser utilizado}{12}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Constru\IeC {\c c}\IeC {\~a}o do modelo matem\IeC {\'a}tico}{13}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Conclus\IeC {\~o}es, limita\IeC {\c c}\IeC {\~o}es e estudos futuros}{17}{subsection.3.3}
\contentsline {section}{\numberline {4}CRONOGRAMA}{18}{section.4}
\contentsline {section}{REFER\IeC {\^E}NCIAS BIBLIOGR\IeC {\'A}FICAS}{20}{table.3}
